from operator import mod
from pyexpat import model
from django.db import models

# Create your models here.
class Users(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=60)
    phone_no = models.CharField(max_length=15)

class Books(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    title = models.CharField(max_length=60)
    content = models.CharField(max_length=200)
    date_published = models.DateField()

class Sales(models.Model):
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    book = models.ForeignKey(Books, on_delete=models.CASCADE)
    date_sale = models.DateField

john = Users()