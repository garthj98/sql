class Users():
    def __init__(self, first_name, last_name, email, phone_no):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone_no = phone_no

class Books():
    def __init__(self, book_ID, user_ID, title, content, date_published)
        self.book_ID = book_ID
        self.user_ID = user_ID
        self.title = title
        self.content = content
        self.date_published = date_published

class Sales():
    def __init__(self, sales_ID, user_ID, book_ID, date_sale):
        self.sales_ID = sales_ID
        self.user_ID = user_ID
        self.book_ID = book_ID
        self.date_sale = date_sale