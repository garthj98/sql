read -p "Enter Key location (if left blank will use garth key): " KEYLOC
if [ -z "$KEYLOC" ]
then 
    KEYLOC=~/.ssh/garth-key-AWS.pem
fi

IPADDRESS=$1
if [ -z "$IPADDRESS" ]
then 
    read -p "You forgot to enter the IP, please enter: " IPADDRESS
fi

ssh -o StrictHostKeyChecking=no -i $KEYLOC ec2-user@$IPADDRESS '

sudo yum update
sudo yum install mariadb-server -y
sudo systemctl enable mariadb
sudo systemctl start mariadb

sudo yum install git-all -y
git clone https://github.com/cwoodruff/ChinookDatabase.git

mysql -u root < /home/ec2-user/ChinookDatabase/Scripts/Chinook_MySql.sql

'